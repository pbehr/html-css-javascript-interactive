window.onload = function() {
    let theButton = document.getElementById('showHideButton');
    let theSection = document.getElementById('articleList');

    function showHideArticles( event ) {
        var action = event.target.innerHTML;
        if (action === 'Hide') {
            theButton.innerHTML = 'Show';
            theSection.classList.add('hidden');
        } else {
            theButton.innerHTML = 'Hide';
            theSection.classList.remove('hidden');
        }
    }

    theButton.onclick = showHideArticles;
}